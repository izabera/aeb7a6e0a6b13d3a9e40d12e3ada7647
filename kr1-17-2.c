#define _GNU_SOURCE
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <string.h>

int main() {

  // https://github.com/coreutils/coreutils/blob/master/src/ioblksize.h
  struct stat st;

  ssize_t obufsiz = 128 * 1024, ibufsiz = 128 * 1024;
  fstat(1, &st);
  if (st.st_blksize > obufsiz) obufsiz = st.st_blksize;
  fstat(0, &st);
  if (st.st_blksize > ibufsiz) ibufsiz = st.st_blksize;

  // this uses raw input and buffered output
  char inbuf[ibufsiz+81], outbuf[obufsiz], *map;
  // this sets the size of the write() buffer used in stdio (in glibc)
  setvbuf(stdout, outbuf, _IOFBF, obufsiz);
  // set a few flags on stdin
  posix_fadvise(0, 0, 0, POSIX_FADV_WILLNEED);
  posix_fadvise(0, 0, 0, POSIX_FADV_SEQUENTIAL);
  
  if (st.st_size > 0 && st.st_size <= ibufsiz) {
    read(0, inbuf, st.st_size);
    map = inbuf;
    goto mmap;
  }
  if (st.st_size > ibufsiz && (map = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, 0, 0)))
mmap:
  {
    char *ptr, *prev = map;
    size_t size = st.st_size;
    while (size && (ptr = memchr(prev, '\n', size))) {
      if (ptr - prev > 80)
        fwrite_unlocked(prev, 1, ptr - prev + 1, stdout);
      size = size - (ptr - prev) - 1;
      prev = ptr + 1;
    }
    if (size) // last line didn't end in \n
      fwrite_unlocked(prev, 1, size, stdout);
  }
  else {

    char *ptr, *prev = inbuf+81;

    for (ssize_t size; (size = read(0, inbuf+81, ibufsiz)) > 0; ) {
      size += inbuf + 81 - prev; // add size of the possible leftovers

innerloop:
      while (size && (ptr = memchr(prev, '\n', size))) {
        if (ptr - prev > 80)
          fwrite_unlocked(prev, 1, ptr - prev + 1, stdout);
        size = size - (ptr - prev) - 1;
        prev = ptr + 1;
      }

      // leftovers
      while (size > 80) {
        // line was too long, keep reading
        fwrite_unlocked(prev, 1, size, stdout);
        if ((size = read(0, prev = inbuf+81, ibufsiz)) > 0) {
          if ((ptr = memchr(prev, '\n', size))) {
            fwrite_unlocked(prev, 1, ptr - prev + 1, stdout);
            size = size - (ptr - prev) - 1;
            prev = ptr + 1;
            goto innerloop;
          }
        }
        else break;
      }
      // move leftovers at the beginning
      memcpy(inbuf+81-size, prev, size);
      prev = inbuf + 81 - size;
    }

    // final leftovers (this only happens if the file doesn't end in \n)
    fwrite_unlocked(prev, 1, inbuf+81-prev, stdout);
  }
}
